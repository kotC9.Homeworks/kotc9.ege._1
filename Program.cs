﻿using System;
using Microsoft.SqlServer.Server;

namespace Ege._1
{
    /*
     * Дан целочисленный массив из 1000 элементов.
     * Элементы массива могут принимать неотрицательные целые значения до 10 000 включительно.
     * Необходимо найти минимальный кратный 3 и минимальный кратный 5 элемент массива.
     * Затем необходимо уменьшить каждый кратный 3 и кратный 5 на соответствующий им минимум.
     * При этом кратные 15 должны быть уменьшены на сумму этих минимумов.
     * В качестве результата необходимо вывести изменённый массив, каждый элемент выводится с новой строчки.
     * Например, для исходного массива из девяти элементов: 5 9 10 11 12 13 14 15 16
     * программа должна вывести следующий массив 0 0 5 11 3 13 14 1 16
     * (Минимальный кратный трём равен 9,
     * минимальный кратный 5 равен 5.
     * Все кратные трём уменьшены на 9,
     * всё кратные пяти уменьшены на 5,
     * все кратные пятнадцати уменьшены на 14)
     */
    internal class Program
    {
        public static void Main(string[] args)
        {
            var array = GenerateArray(100, 0, 10000);

            var minMultiple3 = 9999;
            var minMultiple5 = 10000;
            
            Console.WriteLine("SOURCE: ");
            foreach (var element in array)
            {
                Console.Write($"{element} ");
            }
            
            //поиск минимальных значений кратных 3 и 5
            foreach (var element in array)
            {
                if (element % 3 == 0 && element < minMultiple3)
                {
                    minMultiple3 = element;
                }
                else if (element % 5 == 0 && element < minMultiple5)
                {
                    minMultiple5 = element;
                }
            }

            for (var i = 0; i < array.Length; i++)
            {
                if (array[i] % 15 == 0)
                {
                    array[i] -= (minMultiple3 + minMultiple5);
                }
                else if (array[i] % 5 == 0)
                {
                    array[i] -= minMultiple5;
                }
                else if (array[i] % 3 == 0)
                {
                    array[i] -= minMultiple3;
                }
            }

            Console.WriteLine("RESULT: ");
            foreach (var element in array)
            {
                Console.Write($"{element} ");
            }
            
            Console.WriteLine($"Min multiple 3 = {minMultiple3}");
            Console.WriteLine($"Min multiple 5 = {minMultiple5}");
        }

        private static int[] GenerateArray(uint size, int min, int max)
        {
            if (min > max)
                return null;
            
            var result = new int[size];
            for (var i = 0; i < size; i++)
            {
                result[i] = LockedRandom.GetRandomNumber(min, max);
            }

            return result;
        }
    }
}